<?php
/**
 * Created by PhpStorm.
 * User: Gedeon
 * Date: 2019-02-27
 * Time: 23:07
 */

namespace App\Command;


use App\Service\XmlParser;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ConverterCommand extends Command
{

    private $xmlParser;

    public function __construct(XmlParser $xmlParser)
    {
        $this->xmlParser = $xmlParser;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:convert_currency')
            // the short description shown while running "php bin/console list"
            ->setDescription('Update currency rates')
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command allows update currency data...");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $output->writeln([
            '',
            ' Start update currency...',
            '',
        ]);

        $parsedData = $this->xmlParser->ecbParser();

        if($parsedData && isset($parsedData['error'])){
            $output->writeln([
                '',
                $parsedData['error'],
                '',
            ]);
        }elseif(isset($parsedData['success'])){
            $output->writeln([
                '',
                $parsedData['success'],
                '',
            ]);
        }else{
            $output->writeln([
                '',
                'Something went wrong!',
                '',
            ]);
        }

        $parsedDataCBR = $this->xmlParser->cbrParser();

        if($parsedDataCBR && isset($parsedDataCBR['error'])){
            $output->writeln([
                '',
                $parsedDataCBR['error'],
                '',
            ]);
        }elseif(isset($parsedDataCBR['success'])){
            $output->writeln([
                '',
                $parsedDataCBR['success'],
                '',
            ]);
        }else{
            $output->writeln([
                '',
                'Something went wrong!',
                '',
            ]);
        }

    }
}