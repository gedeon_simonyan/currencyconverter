<?php
/**
 * Created by PhpStorm.
 * User: Gedeon
 * Date: 2019-02-28
 * Time: 21:45
 */

namespace App\Service;


use App\Entity\Currency;
use Doctrine\ORM\EntityManager;

class CurrencyServices
{
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function convertCurrency(string $from,string $to,float $amount)
    {
        $currencyFrom = $this->entityManager->getRepository(Currency::class)->findOneBy(
            ['value' => $from, 'parent_currency' => $to]
        );
        $currencyTo = $this->entityManager->getRepository(Currency::class)->findOneBy(
            ['value' => $to, 'parent_currency' => $from]
        );
        /*
         * If converted currency is our base currency
         * */
        if ($currencyFrom) {
            $output = $currencyFrom->getCoefficient() / floatval($currencyFrom->getAmount()) * floatval($amount);
            /*
             * If needed currency is our base currency
             * */
        } elseif ($currencyTo) {
            $output = floatval($currencyTo->getAmount()) * floatval($amount);
        } else {
            /*
             * If the currencies are from different source
             * */
            $output = $this->notBaseCurrencyConvert($from, $to, $amount);

            if (!$output) {
                return false;
            }
        }

        return round($output, 7);

    }

    private function notBaseCurrencyConvert(string $from,string $to,float $amount)
    {
        $fromCurrency = $this->entityManager->getRepository(Currency::class)->findOneBy(['value' => $from]);
        $toCurrency = $this->entityManager->getRepository(Currency::class)->findOneBy(['value' => $to]);

        if ($fromCurrency && $toCurrency) {
            /*
             * If the 2 currencies are from one source currency
             * */
            if ($fromCurrency->getParentCurrency() == $toCurrency->getParentCurrency()) {
                $currentAmount = $toCurrency->getAmount() / $fromCurrency->getAmount();
                $output = $amount * $currentAmount;
            } else {
                /*
                 * Return Eur to Rub amount
                 * */
                $eurToRub = $this->entityManager->getRepository(Currency::class)->findOneBy(
                    ['value' => 'RUB', 'parent_currency' => 'EUR']
                );

              /*
               * If converted currency source is EUR
               * */
                if ($fromCurrency->getParentCurrency() == 'EUR') {
                    $toAmount = $toCurrency->getAmount();
                    $currentAmount = $eurToRub->getAmount() / $fromCurrency->getAmount() * $toAmount;
                    $output = $currentAmount * $amount;
                } else {
                    /*
                   * If converted currency source is RUB
                   * */
                    $fromAmount = $fromCurrency->getAmount();
                    $eurAmount = 1 / $eurToRub->getAmount();
                    $currentAmount = $fromAmount / $eurAmount;
                    $output = $toCurrency->getAmount() / $currentAmount * $amount;
                }
            }

            return $output;

        } else {

            return false;
        }
    }
}