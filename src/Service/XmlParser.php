<?php
/**
 * Created by PhpStorm.
 * User: Gedeon
 * Date: 2019-02-26
 * Time: 23:02
 */

namespace App\Service;


use App\Entity\Currency;
use App\Repository\CurrencyRepository;
use Doctrine\DBAL\ConnectionException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Symfony\Component\DomCrawler\Crawler;

class XmlParser
{
    private $entityManager;
    private $currencyRepository;

    public function __construct(EntityManager $entityManager,CurrencyRepository $currencyRepository)
    {
        $this->entityManager = $entityManager;
        $this->currencyRepository = $currencyRepository;
    }

        /* Parse currency data from this url https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml
         * and insert data to db for EUR
         * */

    public function ecbParser(){

        $crawlerEcb = new Crawler(file_get_contents('https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml'));
        $crawlerEcb = $crawlerEcb->filterXPath('//default:Cube');

        $currentDate = new \DateTime();
        if($crawlerEcb->count()>0){
            try{
                $this->currencyRepository->truncateTable();
            }catch (\Exception $e){
                return ['error'=>$e->getMessage()];
            }

        }

        foreach ($crawlerEcb as $key=>$domElement) {
            if($key >1){
                $currencyType = $domElement->attributes->item(0)->firstChild->textContent;
                $currencyValue = $domElement->attributes->item(1)->firstChild->textContent;
                $currency =  new Currency();
                $currency->setValue($currencyType);
                $currency->setAmount($currencyValue);
                $currency->setUpdateDate($currentDate);
                $currency->setWebsite('https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml');
                $currency->setParentCurrency('EUR');
                $currency->setCoefficient(1);

                try{
                    $this->entityManager->persist($currency);
                    $this->entityManager->flush();
                }catch (ORMException $exception){
                      return ['error'=>'Can not add data ECB'];
                }

            }
        }

        return ['success'=>'ECB Data inserted'];
    }

        /* Parse currency data from this url https://www.cbr.ru/scripts/XML_daily.asp
         * and insert data to db for RUB
         * */

    public function cbrParser(){

        $crawler = new Crawler(file_get_contents('https://www.cbr.ru/scripts/XML_daily.asp'));
        $crawler = $crawler->filterXPath('//ValCurs/Valute');


        $currentDate = new \DateTime();

        foreach ($crawler as $key=>$domElement) {
            if($key >0){

                $currencyType = $domElement->getElementsByTagName('CharCode')->item(0)->textContent;
                $currencyValue = $domElement->getElementsByTagName('Value')->item(0)->textContent;
                $currencyNominal = $domElement->getElementsByTagName('Nominal')->item(0)->textContent;
                $countedCurrency = $currencyNominal/floatval(str_replace(',','.',$currencyValue));
                $currency =  new Currency();
                $currency->setValue($currencyType);
                $currency->setAmount($countedCurrency);
                $currency->setUpdateDate($currentDate);
                $currency->setWebsite('https://www.cbr.ru/scripts/XML_daily.asp');
                $currency->setParentCurrency('RUB');
                $currency->setCoefficient((int)$currencyNominal);

                try{
                    $this->entityManager->persist($currency);
                    $this->entityManager->flush();
                }catch (ORMException $exception){
                      return ['error'=>'Can not add data CBR'];
                }

            }
        }

        return ['success'=>'CBR Data inserted'];
    }

}