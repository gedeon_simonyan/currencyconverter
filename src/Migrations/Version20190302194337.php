<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190302194337 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE IF NOT EXISTS `currency` (
                          `id` INT(11) NOT NULL AUTO_INCREMENT,
                          `value` VARCHAR(5) NOT NULL,
                          `amount` DOUBLE NOT NULL,
                          `website` VARCHAR(255) NOT NULL,
                          `update_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                          `parent_currency` VARCHAR(5) NOT NULL,
                          `coefficient` INT(11) NOT NULL,
                          PRIMARY KEY (`id`)
                        ) ENGINE=INNODB DEFAULT CHARSET=utf8');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE currency');
    }
}
