<?php
/**
 * Created by PhpStorm.
 * User: Gedeon
 * Date: 2019-02-26
 * Time: 23:18
 */

namespace App\Controller;

use App\Repository\CurrencyRepository;
use App\Service\CurrencyServices;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     * @param CurrencyRepository $currencyRepository
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function home(CurrencyRepository $currencyRepository)
    {
        $currencyList = $currencyRepository->findAllCurrencyTypes();

        return $this->render('Home/Home.html.twig', ['currencyCodes' => $currencyList]);
    }

    /**
     * @Route("/convert", methods={"POST"}, name="convert")
     *
     * @param Request $request
     * @param CurrencyServices $currencyServices
     *
     * @return \Symfony\Component\HttpFoundation\Response | boolean
     */
    public function convert(Request $request, CurrencyServices $currencyServices)
    {
        if ($request->isXmlHttpRequest()) {
            $from = $request->get('from');
            $to = $request->get('to');
            $amount = $request->get('amount');

            if($from && $to && $amount){
                $output = $currencyServices->convertCurrency($from,$to,$amount);

                return new JsonResponse(['converted_amount' => $output]);
            }
        }

        return new JsonResponse(['error' => 'Invalid Request']);
    }

    /**
     * @Route("/api/convert", methods={"GET"}, name="convert_api")
     *
     * @param Request $request
     * @param CurrencyServices $currencyServices
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function convertApi(Request $request, CurrencyServices $currencyServices)
    {
            $from = $request->get('from');
            $to = $request->get('to');
            $amount = $request->get('amount');

            if($from && $to && $amount){
                $output = $currencyServices->convertCurrency($from,$to,$amount);

                if($output){
                    return new JsonResponse(['status'=>'success','data'=>['converted_amount' => $output]]);
                }
            }



        return new JsonResponse(['error' => 'Currency codes are not true']);
    }
}