# Currency converter

This app is made to convert currencies from one to another

## Getting Started

Here you can find the instruction how to install and work with this application and what you need to make tests.
### Prerequisites

What things you need to install the software and how to install them


First, make sure you install [Node.js](https://nodejs.org/en/download/) and also the [Yarn](https://yarnpkg.com/lang/en/docs/install/) package manager.


### Installing

To make run application you need to do following steps. 


```
composer install
```
Configure the database in .env file located in the root folder

Then to create the database run this migration

```
php bin/console doctrine:migrations:execute --up 20190302200513
```
 To import currency data to the database run this command
```
php bin/console app:convert_currency
```
## Running the tests
To run PHPUnit tests run 
```
php bin/phpunit
``` 

# To make changes in style and js and compile them run
```
# yarn encore dev --watch 
```