<?php
/**
 * Created by PhpStorm.
 * User: Gedeon
 * Date: 2019-02-26
 * Time: 23:18
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

class HomeControllerTest extends WebTestCase
{
    public function testConverter()
    {
        $client = static::createClient();
        $jsonEncoder = new JsonEncoder();
        $client->xmlHttpRequest('POST', '/convert',['from'=>'USD','to'=>'EUR','amount'=>10]);
        $this->assertEquals(true, isset($jsonEncoder->decode($client->getResponse()->getContent(),'json')['converted_amount']));

        $client->xmlHttpRequest('GET', '/convert',['from'=>'USD','to'=>'EUR','amount'=>10]);
        $this->assertEquals(405, $client->getResponse()->getStatusCode());

    }

    public function testConverterApi()
    {
        $client = static::createClient();
        $jsonEncoder = new JsonEncoder();

        $client->xmlHttpRequest('GET', '/api/convert',['from'=>'UGG','to'=>'EUR','amount'=>10]);
        $this->assertEquals('Currency codes are not true', $jsonEncoder->decode($client->getResponse()->getContent(),'json')['error']);

        $client->xmlHttpRequest('GET', '/api/convert',['from'=>'USD','to'=>'EUR','amount'=>10]);
        $this->assertEquals('success', $jsonEncoder->decode($client->getResponse()->getContent(),'json')['status']);

        $client->xmlHttpRequest('Post', '/api/convert',['from'=>'UGG','to'=>'EUR','amount'=>10]);
        $this->assertEquals(405,  $client->getResponse()->getStatusCode());

    }


}