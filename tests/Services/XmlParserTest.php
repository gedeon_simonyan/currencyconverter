<?php
/**
 * Created by PhpStorm.
 * User: Gedeon
 * Date: 2019-03-02
 * Time: 00:54
 */

namespace App\Tests\Services;

use PHPUnit\Framework\TestCase;
use Symfony\Component\DomCrawler\Crawler;

class XmlParserTest extends TestCase
{
    public function testParser()
    {
        $crawlerEcb = new Crawler(file_get_contents('https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml'));
        $crawlerEcb = $crawlerEcb->filterXPath('//default:Cube');

        $crawler = new Crawler(file_get_contents('https://www.cbr.ru/scripts/XML_daily.asp'));
        $crawler = $crawler->filterXPath('//ValCurs/Valute');

        if ($crawlerEcb->count() > 0 && $crawler->count()) {

            foreach ($crawler as $key => $domElement) {
                if ($key > 0) {

                    $currencyType = $domElement->getElementsByTagName('CharCode')->item(0)->textContent;
                    // assert that your calculator added the numbers correctly!
                    $this->assertEquals(3,strlen($currencyType));
                }
            }
        }
    }
}
