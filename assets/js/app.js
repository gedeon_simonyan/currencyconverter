/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require('../css/app.css');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
const $ = require('jquery');

$(document).ready(function(){
    let from = $('.convert_from').val();
    let to = $('.convert_to').val();
    let amount = $('.from_amount').val();

    getCurrencyConverted(from, to, amount)
});
$('.amount').keyup(function() {
    let from = $('.convert_from').val();
    let to = $('.convert_to').val();
    let amount = $('.from_amount').val();

    getCurrencyConverted(from, to, amount)
});
$('.changer').change(function() {
    let from = $('.convert_from').val();
    let to = $('.convert_to').val();
    let amount = $('.from_amount').val();

    getCurrencyConverted(from, to, amount)
});
function getCurrencyConverted(from, to, amount) {
    if(!amount){
        $('.from_amount').val('1');
        amount = 1;
    }
    $.post("http://127.0.0.1:8000/convert", {from: from, to: to, amount:amount}, function(result){
        $(".to_amount").val(result.converted_amount);

    });
}
